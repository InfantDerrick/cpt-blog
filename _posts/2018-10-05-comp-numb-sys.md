---
layout:     post
title:      Computer Number Systems
date:       2018-10-05 07:42:29
summary:    Resources for Computer Number Systems.
author:     Sanjit Bhat
categories: comp1
---

Computer number systems is the first of three topics in the December
ACSL contest. In this short post, I'll go through the training material
we used in our 9/28/2018 and 10/5/2018 meetings.

Upon first glance, number systems are wonky.
To make their importance more clear, we ask the following questions:
1. Why does ACSL care so much about them in the first place?
2. Why don't we all use base 10 for everything? Wouldn't it be
more convenient to have one standard way of counting?

To answer some of these questions, I'll refer to
[this](https://betterexplained.com/articles/numbers-and-bases/)
article, which provides a good historic overview.
Overall, the key takeaway is that number systems are
the solution to one of the fundamental human skill: counting things.
As such, they have been around for a long time, and different methods
of counting have evolved over time. Right now, while we use base 10
in everyday life, we also use other bases for different applications.
For example, computers use base 2 because the two states per digit
are naturally expressible in circuits. Clocks use base 60
because 60 seconds make a minute and 60 minutes make an hour.

Now that we have a good foundational understanding of why we need a
topic, we move on to understanding specific problem-solving techniques.
Specifically, the two key techniques to solve most number system problems
are converting from base 10 to base n, and vice-versa. Both are described
in further detail in the problem set.

For practice, try solving the problems linked
[here.]({{ site.url }}/cpt-blog/misc_files/comp-number-systems.pdf)
Think about the process rather than applying memorized techniques:
jot down any information you see or patterns you observe, and view
exploring the problem in any manner as progress.

The above method actually has a name: Computational Thinking.
In our training we'll embrace this idea more,
approaching tough problems together and building these important skills.

